package com.example.demo.api.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WelcomeController {

    @Value("${app.client-name}")
    private String clientName;

    @Value("${app.welcome-message}")
    private String welcomeMessage;

    @GetMapping(path = {"/api/welcome"})
    public String getWelcomeMessage() {
        return clientName + " - " + welcomeMessage;
    }
}
